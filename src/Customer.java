import java.util.*;

public class Customer implements Validation {
    public String Name;
    public String Username;
    public String Password;
    public int age,index;
    public String SSN;
    public String Address;
    public String Email;
    public long Phone;
    public float Balance = 0.0f;
    ArrayList<String> UsersList = new ArrayList<String>();
    ArrayList<String>PasswordList= new ArrayList<String>();
    Scanner scan = new Scanner(System.in);
    public void login(){
        int a =0;
        do{
            System.out.println("Enter Username: ");
            Username=scan.next();
            System.out.println("Enter Password: ");
            Password=scan.next();
            index=UsersList.indexOf(Username);
            if(index!=-1){
                String password1= PasswordList.get(index);
                if(Password.equals(password1)){
                    System.out.println("Login Successful");
                    a=1;
                }
                else{
                    System.out.println("Invalid login details");
                }
            }
        }while(a==0);
    }
    public void deposit(){
        System.out.println("Enter amount to deposit ");
        float depsoitmoney = scan.nextFloat();
        Balance+=depsoitmoney;
        System.out.println("Amount deposited in your account");
    }
    public void withdrawal(){
        System.out.println("Enter amount to withdraw ");
        float withdrawMoney=scan.nextFloat();
        if(withdrawMoney>Balance){
            System.out.println("Insufficient funds ");
        }
        else{
            Balance-=withdrawMoney;
            System.out.println("Transaction Successful");
            System.out.println("Please collect your cash ");
        }
    }
    public void checkBalance(){
        System.out.println("Your balance is "+Balance);
    }
    public void editProfile(){
        System.out.println("Enter your Name");
        Name=scan.next();
        System.out.println("Enter your age");
        age=scan.nextInt();
        scan.next();
        System.out.println("Enter your SSN");
        SSN=scan.next();
        System.out.println("Enter your Address");
        Address=scan.next();
        System.out.println("Enter your Email");
        Email=scan.next();
        System.out.println("Enter your Phone Number");
        Phone=scan.nextLong();
        if(validateSSN(SSN))
        {
        	if(validateAGE(age))
        	{
        		System.out.println("\nYour Profile Updated\n");
                System.out.println("\nUpdated Details\n");
                System.out.println("Name: "+Name);
                System.out.println("Age: "+age);
                System.out.println("Address: "+Address);
                System.out.println("SSN: "+SSN);
                System.out.println("Email: "+Email);
                System.out.println("Phone Number: "+Phone);
        	}
        	else
        	{
        		System.out.println("Invalid Age Your Details are not Updated");
        	}
        }
        else
        {
        	System.out.println("Invalid SSN Your Details are not Updated");
        }

    }
    public void changePassword(){
        int a = 1;
        do{
            System.out.println("Enter old password ");
            String oldpassword = scan.next();
            System.out.println("Enter new password ");
            String newPassword = scan.next();
            System.out.println("confirm password ");
            String confirmPassword = scan.next();
            if(Password.equals(oldpassword)){
                if(newPassword.equals(confirmPassword)){
                    System.out.println("Your password is updated ");
                    a=0;
                }
                else{
                    System.out.println("New password and confirm passwords must be same");
                }
            }
            else{
                System.out.println("Enter your old password correctly");
            }
        }while(a==1);
    }
    public Boolean validateSSN(String SSN){
        if(SSN.length()==9){
            return true;
        }
        return false;
    }
    public Boolean validateAGE(int age){
        if(age>=18){
            return true;
        }
        return false;
    }
    public String toString() {
		return "Customer [name=" + Name + ", userName=" + Username + ", password=" + Password + ", SSN=" + SSN
				+ ", email=" + Email + ", address=" + Address + ", age=" + age + ", phone=" + Phone + ", balance="
				+ Balance + "]";
	}

}
