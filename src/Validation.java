public interface Validation {
    public Boolean validateSSN(String SSN);
	public Boolean validateAGE(int age);
}
