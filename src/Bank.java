import java.util.ArrayList;
import java.util.Scanner;

public class Bank {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String con;
        int cho1;
        ArrayList<Customer> details=new ArrayList<Customer>();
        Customer c=new Customer();
        while(true)
        {
        	System.out.println("1:Login\n2:Open An Account\n3:Exit\nEnter Your Choice");
            cho1=sc.nextInt();
            sc.nextLine();
            if(cho1==1)
            {
            	c.login();
            	Customer c1=details.get(c.index);
                do
                {
                	System.out.println("1:Deposit\n2:Withdrawal\n3:checkBalance\n4:Edit Profile"+
                	        "\n5:Change Password\nEnter your choice");
                	int choice=sc.nextInt();
                	switch(choice)
                	{
                	   case 1:c1.deposit();
                	   		  break;
                	   case 2:c1.withdrawal();
                	   		   break;
                	   case 3:c1.checkBalance();
                	   		  break;
                	   case 4:c1.editProfile();
                	   		  details.set(c.index,c1);
                	          break;
                	   case 5:c1.changePassword();
                	          break;
                	   default: System.out.println("Invalid choice");
                	   			
                	}
                	sc.nextLine();
                	System.out.println("Do you want to continue?");
                	con=sc.nextLine();
                }while(con.equalsIgnoreCase("Yes"));
                if(con.equalsIgnoreCase("No"))
                {
                	System.out.println("Thank You");
                }
            }
            else if(cho1==2)
            {
            	System.out.println("Enter your Name");
                c.Name=sc.nextLine();
                System.out.println("Enter your age");
                c.age=sc.nextInt();
                sc.nextLine();
                System.out.println("Enter your SSN");
                c.SSN=sc.nextLine();
                System.out.println("Enter your Address");
                c.Address=sc.nextLine();
                System.out.println("Enter your Email");
                c.Email=sc.nextLine();
                System.out.println("Enter your Phone Number");
                c.Phone=sc.nextLong();
                sc.nextLine();
            	System.out.println("Enter Username");
                c.Username=sc.nextLine();
                System.out.println("Enter Password");
                c.Password=sc.nextLine();
                if(c.validateSSN(c.SSN))
                {
                	if(c.validateAGE(c.age))
                	{
                		OpenBankAccount o=new OpenBankAccount();
                        o.openAcc();
                        int flag=0;
                        do
                        {
                        	System.out.println("Enter your amount to Deposit and Create account");
                        	c.Balance=sc.nextFloat();
                            if((o.type.equals("Savings") && c.Balance>500)||(o.type.equals("Current") && c.Balance>0))
                            {
                            	flag=1;
                            	c.UsersList.add(c.Username);
                            	c.PasswordList.add(c.Password);
                            	details.add(c);
                            	System.out.println("Your Account is Created");
                            }
                            else
                            {
                            	System.out.println("Your Account is not Created and Invalid amount");
                            	flag=0;
                            }
                        }while(flag==0);
                	}
                	else
                	{
                		System.out.println("Invalid Age We can't open account");
                	}
                }
                else
                {
                	System.out.println("Invalid SSN We can't open account");
                }
                
            }
            else if(cho1==3)
            {
            	sc.close();
            	System.exit(0);
            }
            else
            {
            	System.out.println("Invalid Choice");
            }
        }
    }

    }

